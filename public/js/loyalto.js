$(document).ready(function() {
  $('#btn-group-edit').on('click', '#emp-edit', function(){

    $('#name').removeAttr('readonly');
    $('#salary').removeAttr('readonly');
    $('#age').removeAttr('readonly');

    $('#btn-group-edit').html('');
    $('#btn-group-edit').html(`
        <button type="submit" class="btn btn-success pull-right" id="emp-edit-submit">submit</button>
        <button type="button" class="btn btn-warning pull-right" id="emp-edit-cancel">cancel</button>
      `);
  });
});


$(document).ready(function() {
  $('#btn-group-edit').on('click','#emp-edit-cancel', function(){

    $('#name').prop('readonly', true);
    $('#salary').prop('readonly', true);
    $('#age').prop('readonly', true);

    $('#btn-group-edit').html('');
    $('#btn-group-edit').html(`
      <button type="button" class="btn btn-warning pull-right" id="emp-edit">edit</button>
      `);
    });
});
