@extends('layouts.master')

@section('content')

@if(Session::has('success'))
  <div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> Alert!</h4>
    {{ Session::get('success') }}
  </div>
@elseif(Session::has('error'))
  <div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> Alert!</h4>
    {{ Session::get('error') }}
  </div>
@endif

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Employee
    <small>managing data of employees</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{url('/employee')}}"><i class="fa fa-users"></i> Employee</a></li>
    <li class="active"><a href="#">detail</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Employee Detail</h3>
          <div class="box-tools pull-right">
            <a href="{{url('employee')}}" class="btn btn-md btn-danger"><i class="glyphicon glyphicon-remove"></i> </a>
          </div>
          <div class="clearfix"></div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <form class="form-horizontal form-label-left" method="post" action="{{ url ('employee/update/'.$dataResult[0]['id']) }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-6">Id</label>
              <div class="col-md-6 col-sm-6 col-xs-9">
                <input type="text" id="id" class="form-control" required name="id" value="{{$dataResult[0]['id']}}" readonly>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-6">Name</label>
              <div class="col-md-6 col-sm-6 col-xs-9">
                <input type="text" id="name" class="form-control" required name="name" value="{{$dataResult[0]['employee_name']}}" readonly>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-6">Salary</label>
              <div class="col-md-6 col-sm-6 col-xs-9">
                <input type="text" id="salary" class="form-control" required name="salary" value="{{$dataResult[0]['employee_salary']}}" readonly>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-6">Age</label>
              <div class="col-md-6 col-sm-6 col-xs-9">
                <input type="text" id="age" class="form-control" required name="age" value="{{$dataResult[0]['employee_age']}}" readonly>
              </div>
            </div>

            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-9 col-sm-9 col-xs-12" id="btn-group-edit">
                <button type="button" class="btn btn-warning pull-right" id="emp-edit">edit</button>
              </div>
            </div>
          </form>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>

</div>
</div>
@endsection
