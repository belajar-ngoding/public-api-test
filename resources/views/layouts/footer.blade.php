<footer class="main-footer">
  <div class="pull-right hidden-xs">
    <b>Version</b> 1.0.0
  </div>
  <strong>Copyright &copy; 2020 <a href="https://www.linkedin.com/in/hasbiyallah-s-a5b776136/" target="_blank">Komodo</a>.</strong> All rights
  reserved.
</footer>
