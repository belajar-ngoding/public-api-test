<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="{{url('assets/dist/img/avatar5.png')}}" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>{{auth::user()->username}}</p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>

    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu tree" data-widget="tree">
      <li class="header">MENU</li>

      <li class="{{request()->is('employee') ? 'active' : null}} {{request()->is('employee/*') ? ' active' : null}}">
        <a href="{{url('employee')}}">
          <i class="fa fa-users"></i> <span>Employee</span>
        </a>
      </li>

    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
