@extends('layouts.master')

@section('content')
<!-- Content Header (Page header) -->
@if(Session::has('success'))
  <div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> Alert!</h4>
    {{ Session::get('success') }}
  </div>
@elseif(Session::has('error'))
  <div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> Alert!</h4>
    {{ Session::get('error') }}
  </div>
@endif
<section class="content-header">
  <h1>
    Employee
    <small>managing data of employees</small>
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href=""><i class="fa fa-users"></i> Employee</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Data of Employees</h3>
          <div class="box-tools pull-right">
            <a href="{{url('employee/create')}}" class="btn btn-md btn-success" ><i class="fa fa-user-plus"></i> New </a>
          </div>
          <div class="clearfix"></div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example2" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th style="width:20">#</th>
                <th>Id</th>
                <th>Name</th>
                <th>Salary</th>
                <th>Age</th>
                <th class="col-sm-2">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php $no=1; ?>
              @foreach($dataResult as $data)
              <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $data['id'] }}</td>
                <td>{{ $data['employee_name'] }}</td>
                <td>{{ $data['employee_salary'] }}</td>
                <td>{{ $data['employee_age'] }}</td>
                <td class="text-center">
                  <div class="btn-group" role="group">
                    <!-- view button -->
                    <a href="{{url('employee/detail/'.$data['id'])}}" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-eye-open"></i></a>
                  </div>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>

<!-- Modal Crate -->
<div class="modal fade" id="modal-create" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header modal-info">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-remove"></i></button>
        <h4 class="modal-title text-center" id="myModalLabel" ><i class="fa fa-plus-square"></i> New Employee</h4>
      </div>
      <div class="modal-body">
        {{-- modal content --}}
        <div class="x_panel">
          <div class="x_content">
            <form class="form-horizontal form-label-left" method="post" action="" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Name</label>
                <div class="col-md-7 col-sm-6 col-xs-9">
                  <input type="text" id="name" class="form-control" placeholder="e.g., Jhon Lee" required name="name">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Salary</label>
                <div class="col-md-7 col-sm-6 col-xs-9">
                  <input type="text" id="salary" class="form-control" placeholder="e.g., 12000" required name="salary">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Age</label>
                <div class="col-md-7 col-sm-6 col-xs-9">
                  <input type="text" id="age" class="form-control" placeholder="e.g., 25" required name="age">
                </div>
              </div>

              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="text-center">
                  <button type="reset" class="btn btn-warning">Reset</button>
                  <button type="submit" class="btn btn-success" enabled >Submit</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      {{-- end modal content --}}
    </div>
  </div>
</div>

<div class="modal fade" id="modal-detail" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header modal-info">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-remove"></i></button>
        <h4 class="modal-title text-center" id="myModalLabel" ><i class="fa fa-plus-square"></i> Employee Detail</h4>
      </div>
      <div class="modal-body">
        {{-- modal content --}}
        <div class="x_panel">
          <div class="x_content">
            <form class="form-horizontal form-label-left" method="post" action="" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="form-group hiden">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Id</label>
                <div class="col-md-7 col-sm-6 col-xs-9">
                  <input type="text" id="id" class="form-control" required name="id" readonly>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Name</label>
                <div class="col-md-7 col-sm-6 col-xs-9">
                  <input type="text" id="name" class="form-control" placeholder="e.g., Jhon Lee" required name="name" readonly>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Salary</label>
                <div class="col-md-7 col-sm-6 col-xs-9">
                  <input type="text" id="salary" class="form-control" placeholder="e.g., 12000" required name="salary" readonly>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Age</label>
                <div class="col-md-7 col-sm-6 col-xs-9">
                  <input type="text" id="age" class="form-control" placeholder="e.g., 25" required name="age" readonly>
                </div>
              </div>

              <div class="form-group">
                <div class="text-center" id="edit-btn">
                  <button type="button" id="edit-edit" class="btn btn-primary" hidden="true">Edit</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      {{-- end modal content --}}
    </div>
  </div>
</div>
@endsection
